# LED-Cube

A Controller board for an 8x8x8 LED Cube.

## WORK IN PROGRESS

This is at the moment just an idea and not validated yet. It might work, it might not.

## Output data

CI will try to build some data, but I have not finished scraping the BOM.

- [Schematics PDF](https://gitlab.com/gumulka/LED-Cube/-/jobs/artifacts/main/raw/Output/LED-Cube.pdf?job=documentation)
- [Incomplete STEP file](https://gitlab.com/gumulka/LED-Cube/-/jobs/artifacts/main/raw/Output/LED-Cube.step?job=documentation)
- [Gerber Files](https://gitlab.com/gumulka/LED-Cube/-/jobs/artifacts/main/raw/LED-Cube.zip?job=documentation)

## 3D Models

The 3D Models might not be very accurate, as I just searched for what is
available, therefore especially the USB-C is known to be a bit off. It is a
sacrifice I'm willing to make.

- RJ45 connector from: https://www.we-online.com/en/components/products/WE-LAN-RJ45?sq=74990111211#74990111211
- ESP32 from: https://grabcad.com/library/esp32-c6-wroom-1u-1
- USB-C Connector from: https://grabcad.com/library/usb-type-c-smd-12pin-1
